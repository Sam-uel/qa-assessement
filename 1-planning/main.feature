@assessment
Feature: Main page
  Offer the user our reporters posts encouraging the user to subscribe

  @navbar
  Scenario Outline: User can go through the pages using the navbar
    Given I'm in the main page
    When I click <item> item
    Then I get redirected to <expectedPage> page

    Examples:
      | item             | expectedPage            |
      | BIZ & IT         | /information-technology |
      | TECH             | /gadgets                |
      | SCIENCE          | /science                |
      | POLICY           | /tech-policy            |
      | CARS             | /cars                   |
      | GAMING & CULTURE | /gaming                 |
      | STORE            | /store                  |
      | FORUMS           | /civis                  |

  @navbar
  Scenario: User perform search
    Given I'm in the main page
    When I click the search icon
    And in the search input I write an existing title
    Then I get redirected to search results page with links related

  @sign-in
  Scenario: Sign in with invalid user
    Given I'm in the main page
    When I click on the sign in button
    And I enter a non existent credentials
    Then I get a message saying that I specified an incorrect credential

  @sign-in
  Scenario: Enter sign up page
    Given I'm in the main page
    When I click on the sign in button
    And in sign in collapsible, I click sign up button
    Then I get redirected to register form page

  @posts
  Scenario: See the featured story
    Given I'm in the main page
    When I scroll down the navbar
    Then there is a post highlighted with a tag named "FEATURE STORY"
    And this post has more posters than the other posts

  @posts
  Scenario: the author link in the posts redirects me to their profile information
    Given I'm in the main page
    When I scroll down the navbar
    And I click on an author name
    Then I get redirect to a page with more information about the reporter

  @posts
  Scenario: user loads more stories
    Given I'm in the main page
    When I scroll down to the footer
    And I click on LOAD MORE STORIES BUTTON
    Then I get a redirected to the second page

  @collapsible-navbar
  Scenario Outline: User gets a responsive layout when entering through the phone
    Given I open the page with the device view
    And I'm in the main page
    When I click site menu button
    And in settings section, I click on <layout> layout
    Then I get a responsive layout

    Examples:
      | layout |
      | GRID   |
      | LIST   |

  @collapsible-navbar
  Scenario: User gets a responsive layout when entering mobile site
    Given I open the page with the device view
    And I'm in the main page
    When I click site menu button
    And in navigate section, I click on MOBILE SITE item
    Then I get a responsive layout

  @collapsible-navbar
  Scenario Outline: User changes the site theme
    Given I'm in the main page
    When I click site menu button
    And in settings section, I click on <theme> theme
    Then site gets <theme>

    Examples:
      | theme          |
      | BLACK ON WHITE |
      | WHITE ON BLACK |

  @collapsible-navbar
  Scenario Outline: User changes the site view
    Given I'm in the main page
    When I click site menu button
    And in navigate section, I click on <view> item
    Then site gets <theme> view

    Examples:
      | view        |
      | MOBILE SITE |
      | FULL SITE   |

  @collapsible-navbar
  Scenario Outline: user changes the front page layout
    Given I'm in the main page
    When I click site menu button
    And in settings section, I click on <layout> layout
    Then posts are listed as a <layout>

    Examples:
      | layout |
      | GRID   |
      | LIST   |

  @collapsible-navbar
  Scenario Outline: User applies filters on posts
    Given I'm in the main page
    When I click site menu button
    And in filter by topic section, I click on <item> item
    Then posts are filtered by <item>

    Examples:
      | item             |
      | BIZ & IT         |
      | TECH             |
      | SCIENCE          |
      | POLICY           |
      | CARS             |
      | GAMING & CULTURE |
      | STORE            |
      | FORUMS           |

  @collapsible-navbar
  Scenario Outline: User navigate pages through collapsible navbar
    Given I'm in the main page
    When I click site menu button
    And in navigate section, I click on <item> item
    Then I navigate to <item> page

    Examples:
      | item               |
      | STORE              |
      | SUBSCRIBE          |
      | VIDEOS             |
      | FEATURES           |
      | REVIEWS            |
      | RSS FEEDS          |
      | MOBILE SITE        |
      | ABOUT ARS          |
      | STAFF DIRECTORY    |
      | CONTACT US         |
      | ADVERTISE WITH ARS |
      | REPRINTS           |

  @social-media
  Scenario Outline: User can get our social media profiles
    Given I'm in the main page
    When In follow us section, I click on <socialMedia> icon
    Then I get redirected to our <socialMedia> <profile> profile

    Examples:
      | socialMedia | profile           |
      | facebook    | arstechnica       |
      | twitter     | arstechnica       |
      | youtube     | arstechnicavideos |
      | instagram   | arstechnica       |

  @footer
  Scenario: User subscribes
    Given I'm in the main page
    When I scroll down to the footer
    And I click on sign me up button
    Then I get a redirected to a subscribe page

  @footer
  Scenario Outline: User navigates through pages using the footer
    Given I'm in the main page
    When I scroll down to the footer
    And I click <item> item
    Then I get redirected to <expectedPage> page

    Examples:
      | item              | expectedPage                 |
      | STORE             | /store                       |
      | SUBSCRIBE         | /store/product/subscriptions |
      | ABOUT US          | /about-us                    |
      | RSS FEEDS         | /rss-feeds                   |
      | VIEW MOBILE SITE  | /view?=mobile                |
      | CONTACT US        | /contact-us                  |
      | STAFF             | /staff-directory             |
      | ADVERTISE WITH US | /brands/ars-technica         |
      | REPRINTS          | /reprints                    |

