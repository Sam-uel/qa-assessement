# Practical Challenge for QA Roles at ecoPortal

## Challenge 1 - Test Planning

**Objectives**: Create a document describing a series of test procedures to be made using this [website](https://arstechnica.com) as the subject of the test:

### Scope

- The tests must cover only the main page
- You must include on your validations potential common uses from the perspective of the user
- This isn't intended to be exhaustive, it's to give us a better idea from your work.
- The scenarios and tests cases must be delivered in file inside the folder `1-planning` that you will deliver at the end of the challenges to us.
- You must deliver this file either using [Gerkin language](https://cucumber.io/docs/gherkin/reference/) or a similar natural language in a spreadsheet converted/exported to PDF.
- If you use a spreadsheet, you need to use the following columns to make our assessment easier:
	- Scenario
	- Procedures of test
	- Test Data
	- Expected Results

## Challenge 2 - Test Execution

**Objectives**: Execute the planned tests on the previous challenge, show the findings, create and show possible metrics of execution and results evaluation and specially the defects or improvements you found.

### Scope

- Deliver the documents with your findings on the folder `2-execution`
- You must include the execution summary, total passed cases, total failed cases and other information you think are important.
- It's a follow-up of the planned cases, so show the results on the cases you planned on the previous challenge. It's up to you to judge correct cases you made on the planning phase.
- Also, separate from the metrics, you must create a defects log, in a way that is easy for other stakeholders, like developers, other QA people, product managers, etc).
- Try to think and judge the test results you created using the following principles, so you can properly discuss your findings:
	- Are they really related to the test subject or the main user flow inside a main page news website?
	- Think about the priority. Is it a blocker, that stops the usage? Or it's something that can be fixed in a more planned way?

## Challenge 3 - Test Automation (Automation QA position only)

**Objectives**: Automate one of the scenarios on the same news website, as follows:

```
Scenario: Search a post in the news website
	When I access the news website
	And I search for a post using a existent title
	Then the news website must return the searched post in a results page
```

```
Scenario: User sign-up invalid email
	When I try to sign up or create a new account
	And I try to register a account with a invalid email format
	It returns a error message "The e-mail address you entered is invalid" on a visible place
```

### Scope

- Use the framework of your preference, here, internally we use Rspec and Cypress, but feel free to show one you are comfortable with
	- We are more interested in the concepts you know than the specific tool you used for this challenge
- Deliver the files on another folder called `3-automation`
- Create on the same folder a `README.md` with instructions to execute your tests
- Remember that not everyone have the same environment as you, so try to detail the libraries, applications and commands one might need to fully execute your tests
