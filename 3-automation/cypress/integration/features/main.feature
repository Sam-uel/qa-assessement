Feature: Main page
    Offer the user our reporters posts encouraging the user to subscribe

    Scenario: Search a post in the news website
        When I access the news website
        And I search for a post using an existent title
        Then the news website must return the searched post in a results page

    Scenario: User sign-up invalid email
        When I access the news website
        And I try to sign up or create a new account
        And I try to register an account with a invalid email format
        Then it returns an error message "The e-mail address you entered is invalid" on a visible place


