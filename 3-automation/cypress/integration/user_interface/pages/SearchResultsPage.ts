/// <reference types="cypress" />
import { TestContext } from '../../../integration/utils/TestContext';

export class SearchResultsPage {
    static TITLES = "div.gsc-thumbnail-inside a.gs-title";

    static storeFirstResult() {
        cy.get(this.TITLES).first().invoke('text').then((firstResultText) => {
            TestContext.set('firstResultText', firstResultText.split('...')[0]);
        });
    }

    static validateFirstResult() {
        cy.get(this.TITLES).first().scrollIntoView().invoke('text').then((firstResultText) => {
            //backticks and quotes are removed to avoid unexpected identifier errors
            let titleSearched= TestContext.get("searchedTitle").replace(/[\W_]+/g," ");
            let actualFirstResult = firstResultText.split('...')[0].replace(/[\W_]+/g," ");
            expect(titleSearched).to.contain(actualFirstResult);
        });
    }
}