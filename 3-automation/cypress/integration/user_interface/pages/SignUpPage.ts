/// <reference types="cypress" />

import { PageTitle } from "../components/PageTitle";

export class SignUpPage {
    static USERNAME= "#username";
    static PASSWORD = "#new_password";
    static PASSWORD_CONFIRM = "#password_confirm";
    static EMAIL = "input#email";
    static EMAIL_CONFIRM = "input#email_confirm";
    static SUBMIT = "input[type=submit]";
    static EMAIL_ERROR = "div.error a[href='#email']";

    static validatePage() {
        PageTitle.validateIsVisible("Registration");
    }

    static SignUpUsing(user: any) {
        cy.get(this.USERNAME).type(user.name);
        cy.get(this.PASSWORD).type(user.password);
        cy.get(this.PASSWORD_CONFIRM).type(user.password);
        cy.get(this.EMAIL).type(user.email);
        cy.get(this.EMAIL_CONFIRM).type(user.email);
        cy.get(this.SUBMIT).click();
    }

    static validateEmailErrorMessage(expectedErrorMessage) {
        PageTitle.validateIsVisible(expectedErrorMessage); 
 
    }
}