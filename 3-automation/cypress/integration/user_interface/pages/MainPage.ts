/// <reference types="cypress" />
import { SearchBar } from '../components/SearchBar';
import { SignInForm } from '../components/SignInForm';
import { PageTitle } from '../components/PageTitle';
import { TestContext } from '../../../integration/utils/TestContext';

export class MainPage {
    static ARTICLE_HEADERS = 'li.article header a';

    static validatePage() {
        cy.get(this.ARTICLE_HEADERS).should('be.visible')
        .first()
        .scrollIntoView();
    }

    static searchExistingTitle() {
        let searchedTitle:string;
        cy.get(this.ARTICLE_HEADERS).first().invoke('text').then((firstHeader) => {
            SearchBar.searchFor(firstHeader);
            TestContext.set('searchedTitle', firstHeader);
        });
    }

    static openRegisterPage() {
        SignInForm.openRegisterPage();
    }
}