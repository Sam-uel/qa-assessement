/// <reference types="cypress" />
export class SearchBar {
    static SEARCH_ICON = 'a.search-toggle';
    static SEARCH_INPUT = 'input#hdr_search_input';

    static searchFor(input: string) {
        cy.get(this.SEARCH_ICON).click();
        cy.get(this.SEARCH_INPUT).type(`${input}{enter}`, {force:true});
    }
}