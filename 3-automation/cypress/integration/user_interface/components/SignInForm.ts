export class SignInForm {
    static SIGN_IN_DROPDOWN = "#header-account";
    static SIGN_UP_BUTTON = "a.signup-btn"

    static openRegisterPage() {
        cy.get(this.SIGN_IN_DROPDOWN).click();
        cy.get(this.SIGN_UP_BUTTON).click();
    }
}