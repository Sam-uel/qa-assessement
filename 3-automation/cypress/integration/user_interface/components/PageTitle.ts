/// <reference types="cypress" />

export class PageTitle {
    static validateIsVisible(title: string) {
      cy.get(":root > body").contains(title).should('be.visible')
        .first()
        .scrollIntoView();
    }
  }
  