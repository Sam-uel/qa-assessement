import { When, Then } from 'cypress-cucumber-preprocessor/steps'
import { MainPage } from '../../integration/user_interface/pages/MainPage';
import { SignUpPage } from '../../integration/user_interface/pages/SignUpPage';
import { SearchResultsPage } from '../../integration/user_interface/pages/SearchResultsPage';

When('I access the news website', () => {
    cy.visit('/#',{timeout:60000});
    MainPage.validatePage();  
});

When('I search for a post using an existent title', () => {
    MainPage.searchExistingTitle();
})

Then('the news website must return the searched post in a results page', () => {
  SearchResultsPage.validateFirstResult();
})

When('I try to sign up or create a new account', () => {
    MainPage.openRegisterPage();  
    SignUpPage.validatePage();
});

When('I try to register an account with a invalid email format', () => {
    cy.fixture('invalid-user').then((invalidUser) => {
        SignUpPage.SignUpUsing(invalidUser);
    });
});

Then('it returns an error message {string} on a visible place', (errorMessage) => {
      SignUpPage.validateEmailErrorMessage(errorMessage);
});
