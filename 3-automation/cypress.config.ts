import { defineConfig } from 'cypress'

export default defineConfig({
  chromeWebSecurity: false,
  e2e : {
    specPattern : "cypress/integration/features/*.feature",
    baseUrl: "https://arstechnica.com",
    supportFile : false,
    setupNodeEvents(on, config) {
      const cucumber = require('cypress-cucumber-preprocessor').default
      const options = {
        typescript: require.resolve('typescript'),
      };
      on('file:preprocessor', cucumber(options));
    },
  }
})