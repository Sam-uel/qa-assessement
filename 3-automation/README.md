# Arstechnica e2e testing

## Quick Start

### Execution
- node needs to be installed
```bash
# install deps
$ npm install
# execute assessement scenarios
$ npm run cypress:run --feature=main
```
The evidence of the execution can be seen in a video in ./cypress/videos.main.feature.mp4
>If you are using Visual Studio Code is recommended to use Snippets and Syntax Highlight for Gherkin (Cucumber) from Euclidity.
 
### Cypress UI tests
* Edit `cypress.config.ts` file in the root directory. If testing locally, set the application host through baseUrl property. If testing using a CI tool, add --config baseUrl={dev host} to cypress:run script.

Cypress has a user interface which allows you to select which spec file to run and then it runs the selected tests showing the script steps.

```bash
# Run cypress using GUI
$ npm run cypress:open
```
Then, in the interface, select E2E as testing type, and your browser of preference, and features will be listed to selection.

Or, through the commandline.
```bash
# Run cypress tests through commandline
$ npm run cypress:run [OPTIONS]
```
#### OPTIONS

* --feature=".*" : runs all tests
* --feature=main : runs `cypress/integration/features/main.feature`
* --feature="{main,search}" : runs `cypress/integration/features/main.feature` and `cypress/integration/features/search.feature`